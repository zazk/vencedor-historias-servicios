<?php
require './vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as DB;
use CoolCaptcha\Captcha ;

date_default_timezone_set('America/Lima');
error_reporting(E_ALL);
ini_set('display_errors', 1);
//****************SESIONES********************
session_cache_limiter(false);
//session_start();   
//********************************************
// // Allow from any origin
// if (isset($_SERVER['HTTP_ORIGIN'])) {
//     header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
//     header('Access-Control-Allow-Credentials: true');
//     header('Access-Control-Max-Age: 86400');    // cache for 1 day
// }
// // Access-Control headers are received during OPTIONS requests
// if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

//     if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
//         header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

//     if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
//         header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

// }
  
//********************************************
$app = new \Slim\Slim(array(
    'debug' => true,
    'view' => new \Slim\Views\Twig(),
    'templates.path' => './app/views/'
));

 
// ******************RUTAS********************
$app->get('/', function() use ($app) {
    //print_r($app);
    $app->render('index.html');// "Services Working" ;
}); 

//=============================================

$app->get('/app-ranking', function() use ($app) {
    //print_r($app);
    //Change
    //echo "Ranking";

    $app->render('ranking.html');// "Services Working" ;

}); 

$app->get('/app-premios', function() use ($app) {
    //print_r($app);
    //Change
    $app->render('premios.html');// "Services Working" ;
    //$app->render('index.html');// "Services Working" ;
}); 

$app->get('/app-ganadores', function() use ($app) {
    //print_r($app);
    //Change

    $app->render('ganadores.html');// "Services Working" ;
    //$app->render('index.html');// "Services Working" ;
}); 

$app->get('/app-terminos', function() use ($app) {
    //print_r($app);
    //Change

    $app->render('terminos.html');// "Services Working" ;
}); 

//=================================================

$app->post('/registrar-usuario', function() use ($app) {
    //$response = $app->response();
    //$response->header('Access-Control-Allow-Origin', '*');
    //print_r($app->request()->post());exit;


    $app->response->headers->set('Content-Type', 'application/json');
    $p = (object) $app->request()->post();

    //print_r($p);exit;

    if( $p->nombres && $p->email ){
        DB::insert(
            "INSERT INTO usuarios (nombres, email, password, telefono, fecha_creacion) 
            VALUES (?, ?, ?, ?, now() )", 
            array($p->nombres, $p->email, $p->password,$p->telefono));

        $id = DB::getPdo()->lastInsertId();
        /*
        $id = DB::table('usuarios')->insertGetId(
            ['email' => $email, 'nombres' => $nombres, ]
        );
        */
        //$id = DB::table('someTable')->insertGetId( ['field' => Input['data']);
        if ( !$id) {
            echo json_encode( array('error' => true , 'message' => 'No se ha logrado registrar al usuario' ));
        }
        else{
            $results = DB::select(
                "SELECT * 
                FROM usuarios 
                WHERE id = ? ", 
                array($id)
            );

            if( count($results) ){
                echo json_encode( array('success' => true , 'usuario' => $results[0] ));
            }else{
                echo json_encode( array('error' => true , 'message' => 'No se encuentra el usuario.'  ));
            }
            //echo json_encode( array('success' => true , 'usuario' => $results[0] ));
        }
        //echo json_encode( array('success' => true , 'id' => $id ));
    }else{
        echo json_encode( array('error' => true , 'message' => 'todos los campos son obligatorios' ));
    }
    http_response_code(200);
}); 


$app->post('/registrar-usuario-fb', function() use ($app) {
    //$response = $app->response();
    //$response->header('Access-Control-Allow-Origin', '*');
    //print_r($app->request()->post());exit;

    $app->response->headers->set('Content-Type', 'application/json');
    $p = (object) $app->request()->post();

    if( $p->nombres && $p->email && $p->telefono &&  $p->fbid  ){

        $id = 0;

        $results = DB::select(
                "SELECT * 
                FROM usuarios 
                WHERE fbid = ? ", 
                array($p->fbid)
            );

        if( count( $results) ){
            DB::update(
                "UPDATE usuarios 
                SET  email =?, telefono=? 
                WHERE fbid = ?", 
                array(  $p->email, $p->telefono ,$p->fbid) 
            );

            $id = $results[0]->id;

        }else{

            DB::insert(
                "INSERT INTO usuarios (nombres, email, telefono, fbid, fecha_creacion) 
                VALUES (?, ?, ?, ?, now() )", 
                array($p->nombres, $p->email,$p->telefono,$p->fbid));

            $id = DB::getPdo()->lastInsertId();


        }

        //$id = DB::table('someTable')->insertGetId( ['field' => Input['data']);
        if ( !$id) {
            echo json_encode( array('error' => true , 'message' => 'No se ha logrado registrar al usuario' ));
        }
        else{
            $results = DB::select(
                "SELECT * 
                FROM usuarios 
                WHERE id = ? ", 
                array($id)
            );

            if( count($results) ){
                echo json_encode( array('success' => true , 'usuario' => $results[0] ));
            }else{
                echo json_encode( array('error' => true , 'message' => 'No se encuentra el usuario.'  ));
            }

        }
    }else{
        echo json_encode( array('error' => true , 'message' => 'Todos los campos son obligatorios' ));
    }
    http_response_code(200);
}); 



$app->post('/actualizar-usuario', function() use ($app) {
    //print_r($app->request()->post());exit;
    $app->response->headers->set('Content-Type', 'application/json');

    $p = (object) $app->request()->post();

    if( $p->nombres && $p->email ){
        DB::update(
            "UPDATE usuarios 
            SET nombres = ?, email =?, password=?, telefono=? , fbid=?
            WHERE id = ?", 
            array($p->nombres, $p->email, $p->password,$p->telefono ,$p->fbid, $p->id)
            );

        echo json_encode( array('success' => true  ));
    }else{
        echo json_encode( array('error' => true , 'message' => 'todos los campos son obligatorios' ));
    }
}); 


$app->post('/actualizar-nivel', function() use ($app) {

    define("MAX_COMPARTIDO_APLICACION", 8);
    define("MAX_COMPARTIDO_CANCION", 4);
    //print_r($app->request()->post());exit;
    $app->response->headers->set('Content-Type', 'application/json');

    $p = (object) $app->request()->post();

    if( $p->nivel && isset($p->id) ){


        $results = DB::select(
            "SELECT *  
            FROM usuarios 
            WHERE id = ? ", 
            array($p->id)
        );

        //echo strpos($p->nivel,"-"); exit;


        if( is_numeric( $p->nivel) && ( abs($p->nivel) <= 8) ){ 
            // Si es Nivel
            DB::update(
                "UPDATE 
                    usuarios
                SET 
                    nivel = ?,
                    puntos = ?

                WHERE id = ?", 
                array( $p->nivel, ( $results[0]->puntos + ( $p->nivel * 100 ) ), $p->id ) 
            );
            $results = DB::select(
                "SELECT *  
                FROM usuarios 
                WHERE id = ? ", 
                array($p->id)
            );
            echo json_encode( array('success' => true , 'usuario' => $results[0] ));

        }elseif ( $p->nivel == "app"){
            // Si es Compartir Aplicación
            $compartido = DB::select(
                "SELECT *  
                FROM compartidos 
                WHERE 
                    id_usuario = ? 
                AND tipo = ?", 
                array($p->id, 'app')
            );


            if( count($compartido) ){

                $can = ( ( $compartido[0]->cantidad < MAX_COMPARTIDO_APLICACION  ) );
                if( $can ){

                    $c = $compartido[0];
                    DB::update(
                        "UPDATE 
                            compartidos
                        SET 
                            cantidad = ?

                        WHERE id = ?", 
                        array( $c->cantidad + 1 , $c->id ) 
                    );

                    DB::update(
                        "UPDATE 
                            usuarios
                        SET 
                            puntos = ?

                        WHERE id = ?", 
                        array( ( $results[0]->puntos + 50 ), $p->id ) 
                    );


                    $results = DB::select(
                        "SELECT *  
                        FROM usuarios 
                        WHERE id = ? ", 
                        array($p->id)
                    );

                    echo json_encode( array('success' => true , 'usuario' => $results[0] ));

                }else{
                    echo json_encode( array('error' => true , 'message' => "No se puede compartir aplicación." ));
                }

            }else{
 
                DB::update(
                    "UPDATE 
                        usuarios
                    SET 
                        puntos = ?

                    WHERE id = ?", 
                    array( ( $results[0]->puntos + 50 ), $p->id ) 
                );

                DB::insert(
                    "INSERT INTO compartidos (id_usuario, tipo, cantidad,fecha_creacion) 
                    VALUES (?, ?, ?, now() )", 
                array($p->id, $p->nivel,1) );

                $results = DB::select(
                    "SELECT *  
                    FROM usuarios 
                    WHERE id = ? ", 
                    array($p->id)
                );

                echo json_encode( array('success' => true , 'usuario' => $results[0] ));

            }



        } elseif(  strpos($p->nivel,'-')  ){

            // Si es Compartir Canción
            
            list($tipo, $nivel) = explode( "-", $p->nivel);
            $valid =  ( is_numeric( $nivel )  && ( abs($nivel) < 9 && abs($nivel)  > 0) ) ;


            if( $tipo != "cancion" &&  !$valid ){

                //$app->response->headers->set('Content-Type', 'application/json');
                echo json_encode( array('error' => true , 'message' => "No se puede compartir canción." ));            
                //http_response_code(200);
                //exit;
            }else{

                $compartido = DB::select(
                    "SELECT *  
                    FROM compartidos 
                    WHERE 
                        id_usuario = ? 
                    AND tipo = ?", 
                    array($p->id, $p->nivel)
                );

                if( count($compartido) ){

                    $can = ( ( $compartido[0]->cantidad < MAX_COMPARTIDO_CANCION  ) );
                    if( $can ){

                        $c = $compartido[0];
                        DB::update(
                            "UPDATE 
                                compartidos
                            SET 
                                cantidad = ?

                            WHERE id = ?", 
                            array( $c->cantidad + 1 , $c->id ) 
                        );

                        DB::update(
                            "UPDATE 
                                usuarios
                            SET 
                                puntos = ?

                            WHERE id = ?", 
                            array( ( $results[0]->puntos + 25 ), $p->id ) 
                        );

                        $results = DB::select(
                            "SELECT *  
                            FROM usuarios 
                            WHERE id = ? ", 
                            array($p->id)
                        );

                        echo json_encode( array('success' => true , 'usuario' => $results[0] ));

                    }else{

                        //$app->response->headers->set('Content-Type', 'application/json');
                        echo json_encode( array('error' => true , 'message' => "No se puede compartir canción." ));              
                        //http_response_code(200);
                        //die();
                    }
                }else{

                    DB::update(
                        "UPDATE 
                            usuarios
                        SET 
                            puntos = ?

                        WHERE id = ?", 
                        array( ( $results[0]->puntos + 25 ), $p->id ) 
                    );

                    DB::insert(
                        "INSERT INTO compartidos (id_usuario, tipo, cantidad,fecha_creacion) 
                        VALUES (?, ?, ?, now() )", 
                    array($p->id, $p->nivel,1) );

                    $results = DB::select(
                        "SELECT *  
                        FROM usuarios 
                        WHERE id = ? ", 
                        array($p->id)
                    );

                    echo json_encode( array('success' => true , 'usuario' => $results[0] ));

                }

               }

        }else{

            echo json_encode( array('error' => true , 'message' => "Ha repetido este nivel." ));
            //http_response_code(200);
            //exit;

        }


        // if( count($results) ){
        //     echo json_encode( array('success' => true , 'usuario' => $results[0] ));
        // }else{
        //     echo json_encode( array('error' => true , 'message' => 'No se encuentra el usuario.'  ));
        // }

 
    }else{
        echo json_encode( array('error' => true , 'message' => 'todos los campos son obligatorios' ));
    }

    http_response_code(200); 
}); 


$app->post('/login-usuario', function() use ($app) {
    //print_r($app->request()->post());exit;
    $p = (object) $app->request()->post();
    //header("Content-Type: application/json");
    $app->response->headers->set('Content-Type', 'application/json');

    if( $p->email && $p->password ){
        $results = DB::select(
            "SELECT * 
            FROM usuarios 
            WHERE email = ? AND password = ?", 
            array($p->email, $p->password)
            );
        if( count($results) ){
            echo json_encode( array('success' => true , 'usuario' => $results[0] ));
        }else{
            echo json_encode( array('error' => true , 'message' => 'Usuario/Constraseña incorrecto.'  ));
        }

    }else{
        echo json_encode( array('error' => true , 'message' => 'todos los campos son obligatorios' ));
    }
}); 



$app->post('/actualizar-usuario', function() use ($app) {
    //print_r($app->request()->post());exit;
    $app->response->headers->set('Content-Type', 'application/json');

    $p = (object) $app->request()->post();

    if( $p->nombres && $p->email ){
        DB::update(
            "UPDATE usuarios 
            SET nombres = ?, email =?, password=?, telefono=? , fbid=?
            WHERE id = ?", 
            array($p->nombres, $p->email, $p->password,$p->telefono ,$p->fbid, $p->id)
            );

        echo json_encode( array('success' => true  ));
    }else{
        echo json_encode( array('error' => true , 'message' => 'todos los campos son obligatorios' ));
    }
}); 


$app->post('/actualizar-nivel', function() use ($app) {
    //print_r($app->request()->post());exit;
    $app->response->headers->set('Content-Type', 'application/json');

    $p = (object) $app->request()->post();

    if( $p->nivel && $p->id ){
        DB::update(
            "UPDATE usuarios 
            SET nivel = ?
            WHERE id = ?", 
            array($p->nivel, $p->id)
            );

        echo json_encode( array('success' => true  ));
    }else{
        echo json_encode( array('error' => true , 'message' => 'todos los campos son obligatorios' ));
    }
}); 


$app->post('/ranking', function() use ($app) {
    //print_r($app->request()->post());exit;
    $app->response->headers->set('Content-Type', 'application/json');

    $p = (object) $app->request()->post();
    //header("Content-Type: application/json");
    if( $p->id ){
        $results = DB::select(
            "SELECT puntos, nombres, email 
            FROM usuarios 
            ORDER BY puntos 
            DESC");
        if( count($results) ){
            echo json_encode( array('success' => true , 'usuarios' => $results ));
        }else{
            echo json_encode( array('error' => true , 'message' => 'No existen resultados.'  ));
        }

    }else{
        echo json_encode( array('error' => true , 'message' => 'Todos los campos son obligatorios' ));
    }
}); 

$app->post('/puntos', function() use ($app) {
    //print_r($app->request()->post());exit;
    $app->response->headers->set('Content-Type', 'application/json');

    $p = (object) $app->request()->post();
    //header("Content-Type: application/json");
    if( $p->id ){
        $results = DB::select(
            "SELECT * 
            FROM usuarios 
            WHERE id = ? ", array($p->id ));
        if( count($results) ){
            echo json_encode( array('success' => true , 'usuario' => $results[0] ));
        }else{
            echo json_encode( array('error' => true , 'message' => 'Usuario no registrado.'  ));
        }

    }else{
        echo json_encode( array('error' => true , 'message' => 'Todos los campos son obligatorios' ));
    }
}); 


$app->get('/estrofas', function() use ($app) {
    //print_r($app->request()->post());exit;
    $app->response->headers->set('Content-Type', 'application/json');

    $p = (object) $app->request()->post();
    
    $niveles = DB::select(
            "SELECT 
            CONCAT(cancion, '-', estrofa) nivel, detalle 
            FROM estrofas ");

    foreach ($niveles as $key => $value) {
        $obj[$value->nivel] = $value->detalle;
        # code...
    }

    echo json_encode( $obj );
}); 


$app->run();

?>